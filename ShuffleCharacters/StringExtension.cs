﻿using System;

namespace ShuffleCharacters
{
#pragma warning disable
    public static class StringExtension
    {
        public static string ShuffleChars(string source, int count)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw new ArgumentException("Source string is null or empty or white spaces.");
            }

            if (string.IsNullOrWhiteSpace(source))
            {
                throw new ArgumentException("Source string is null or empty or white spaces.");
            }

            if (count < 0)
            {
                throw new ArgumentException("Count of iterations is less than 0.");
            }

            count %= 6;

            string buffer1 = string.Empty, buffer2 = string.Empty, result = source;

            for (int k = 0; k < count; k++)
            {
                buffer1 = string.Empty;
                buffer2 = string.Empty;
                result = string.Empty;

                for (int i = 0; i < source.Length; i++)
                {
                    if ((i + 1) % 2 == 1)
                    {
                        buffer1 = buffer1.Insert(buffer1.Length, source[i].ToString());
                    }
                    else
                    {
                        buffer2 = buffer2.Insert(buffer2.Length, source[i].ToString());
                    }
                }

                result = buffer1 + buffer2;
                source = result;
            }

            return result;
        }
    }
}
